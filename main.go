package main

import (
	"log"
	"net/http"
	"store-web-server/internal/pkg/routes"
)

func main() {
	routes.Init()
	log.Fatal(http.ListenAndServe(":8081", nil))
}
