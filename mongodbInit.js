conn = new Mongo();

try {
	db = conn.getDB('storeUsers');
	db.createUser({
		user: "usersAdmin",
		pwd: "usersAdmin",
		roles: [{ role: "readWrite", db: "storeUsers" }]
	});
} catch (error) {
	print(error.message)
}

try {
	db = conn.getDB('testStore');
	db.createUser({
		user: "readUser",
		pwd: "readUser",
		roles: [{ role: "read", db: "testStore" }]
	});
	db.createUser({
		user: "admin",
		pwd: "readWrite",
		roles: [{ role: "readWrite", db: "testStore" }]
	});
} catch (error) {
	print(error.message)
}
