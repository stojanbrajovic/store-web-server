package routes

import "net/http"

func getAuthToken(r *http.Request) string {
	return r.Header.Get("Authorization")
}
