package routes

import "net/http"

func setupCORSResponse(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Authorization")
	w.Header().Set("Access-Control-Allow-Methods", "DELETE, PATCH")
}

func handleRoute(route string, handler http.HandlerFunc) {
	corsHandler := func(w http.ResponseWriter, r *http.Request) {
		setupCORSResponse(w)
		if r.Method == "OPTIONS" {
			return
		}

		handler(w, r)
	}

	http.HandleFunc(route, corsHandler)
}
