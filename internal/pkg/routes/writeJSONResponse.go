package routes

import (
	"encoding/json"
	"net/http"
)

func writeJSONResponse(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		handleError(w, err)
	}
}
