package routes

import (
	"encoding/json"
	"net/http"
	"store-web-server/internal/pkg/models"
	"store-web-server/internal/pkg/mongodb"
	"strings"
)

func createItem(w http.ResponseWriter, r *http.Request) {
	var item models.Item
	err := json.NewDecoder(r.Body).Decode(&item)

	if err != nil {
		handleError(w, err)
		return
	}

	id, err := mongodb.CreateItem(item, getAuthToken(r), getStoreName(r))

	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, id)
}

func getItem(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	item, err := mongodb.GetItem(id, getStoreName(r))

	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, item)
}

func getManyItems(w http.ResponseWriter, r *http.Request) {
	idsString := r.URL.Query().Get("ids")
	ids := strings.Split(idsString, ",")
	items, err := mongodb.GetManyItems(ids, getStoreName(r))

	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, items)
}

func deleteItem(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	err := mongodb.Delete(id, mongodb.ItemsCollection, getStoreName(r), getAuthToken(r))

	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, "Success")
}

func updateItem(w http.ResponseWriter, r *http.Request) {
	var item models.Item
	err := json.NewDecoder(r.Body).Decode(&item)

	if err != nil {
		handleError(w, err)
		return
	}

	err = mongodb.UpdateItem(item, getStoreName(r), getAuthToken(r))
	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, "Success")
}

func itemHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		createItem(w, r)
	} else if r.Method == "GET" {
		getItem(w, r)
	} else if r.Method == "DELETE" {
		deleteItem(w, r)
	} else if r.Method == "PATCH" {
		updateItem(w, r)
	} else {
		handleInvalidRequestMethod(w)
	}
}

func fullItemDataHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		id := r.URL.Query().Get("id")
		result, err := mongodb.GetFullItemData(id, getStoreName(r))
		if err != nil {
			handleError(w, err)
			return
		}

		writeJSONResponse(w, result)
	} else {
		handleInvalidRequestMethod(w)
	}
}

func getStoreItemsHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		handleInvalidRequestMethod(w)
		return
	}

	result, err := mongodb.GetStoreItems(getStoreName(r))
	if err != nil {
		handleError(w, err)
	}

	writeJSONResponse(w, result)
}

func initItemRoutes() {
	handleRoute("/item", itemHandler)
	handleRoute("/fullItemData", fullItemDataHandler)
	handleRoute("/storeItems", getStoreItemsHandler)
}
