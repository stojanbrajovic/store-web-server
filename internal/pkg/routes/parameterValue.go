package routes

import (
	"encoding/json"
	"net/http"
	"store-web-server/internal/pkg/models"
	"store-web-server/internal/pkg/mongodb"
)

func parameterValueHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var paramValue models.ParameterValue
		err := json.NewDecoder(r.Body).Decode(&paramValue)
		if err != nil {
			handleError(w, err)
			return
		}
		id, err := mongodb.CreateParameterValue(paramValue, getStoreName(r), getAuthToken(r))
		if err != nil {
			handleError(w, err)
			return
		}
		writeJSONResponse(w, id)
	} else if r.Method == "GET" {
		id := r.URL.Query().Get("id")
		paramValue, err := mongodb.GetParameterValue(id, getStoreName(r), getAuthToken(r))
		if err != nil {
			handleError(w, err)
			return
		}
		writeJSONResponse(w, paramValue)
	} else if r.Method == "DELETE" {
		deleteSingleStoreObject(w, r, mongodb.ParameterValuesCollection)
	} else {
		handleInvalidRequestMethod(w)
	}

}
