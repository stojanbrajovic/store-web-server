package routes

import (
	"encoding/json"
	"net/http"
	"store-web-server/internal/pkg/models"
	"store-web-server/internal/pkg/mongodb"
)

func getSingleStoreObject(w http.ResponseWriter, r *http.Request, collection string, obj interface{}, dbObject *models.DatabaseObject) {
	err := mongodb.GetOne(r.URL.Query().Get("id"), collection, getStoreName(r), obj, dbObject)

	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, obj)
}

func createStoreObject(w http.ResponseWriter, r *http.Request, collection string, obj interface{}) {
	err := json.NewDecoder(r.Body).Decode(obj)

	if err != nil {
		handleError(w, err)
		return
	}

	id, err := mongodb.Create(obj, collection, getStoreName(r), getAuthToken(r))

	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, id)
}

func deleteSingleStoreObject(w http.ResponseWriter, r *http.Request, collection string) {
	id := r.URL.Query().Get("id")
	err := mongodb.Delete(id, collection, getStoreName(r), getAuthToken(r))

	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, "Delete success")
}

func basicStoreObjectRouteHandler(w http.ResponseWriter, r *http.Request, collection string, model interface{}, dbObject *models.DatabaseObject) {
	if r.Method == "POST" {
		createStoreObject(w, r, collection, model)
	} else if r.Method == "GET" {
		getSingleStoreObject(w, r, collection, model, dbObject)
	} else if r.Method == "DELETE" {
		deleteSingleStoreObject(w, r, collection)
	} else {
		handleInvalidRequestMethod(w)
	}
}
