package routes

import (
	"net/http"
	"store-web-server/internal/pkg/models"
	"store-web-server/internal/pkg/mongodb"
)

// Init - initialize all routes
func Init() {
	initUserRoutes()
	initAuthRoutes()
	initItemRoutes()
	http.HandleFunc("/parameter", func(w http.ResponseWriter, r *http.Request) {
		var param models.Parameter
		basicStoreObjectRouteHandler(w, r, mongodb.ParametersCollection, &param, &param.DatabaseObject)
	})
	http.HandleFunc("/parameterValue", parameterValueHandler)
}
