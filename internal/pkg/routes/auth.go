package routes

import (
	"encoding/json"
	"errors"
	"net/http"
	"store-web-server/internal/pkg/mongodb"
)

type loginData struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

var errInvalidUsernamePass = errors.New("Invalid username/password")

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		handleInvalidRequestMethod(w)
		return
	}

	var reqData loginData
	err := json.NewDecoder(r.Body).Decode(&reqData)
	if err != nil {
		handleError(w, err)
		return
	}

	tokenString, err := mongodb.Login(reqData.Username, reqData.Password)
	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, tokenString)
}

func isTokenValidHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		handleInvalidRequestMethod(w)
		return
	}

	tokenString := r.Header.Get("Authorization")
	_, err := mongodb.ValidateToken(tokenString)

	if err != nil {
		handleError(w, errors.New("Invalid token"))
		return
	}

	writeJSONResponse(w, "Token valid")
}

func initAuthRoutes() {
	handleRoute("/isTokenValid", isTokenValidHandler)
	handleRoute("/login", loginHandler)
}
