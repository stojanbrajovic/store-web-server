package routes

import (
	"errors"
	"net/http"
)

func handleError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func handleInvalidRequestMethod(w http.ResponseWriter) {
	handleError(w, errors.New("Invalid request method"))
}
