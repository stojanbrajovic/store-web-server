package routes

import (
	"encoding/json"
	"errors"
	"net/http"
	"store-web-server/internal/pkg/models"
	"store-web-server/internal/pkg/mongodb"
)

func createUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		handleError(w, err)
		return
	}

	token, err := mongodb.CreateStoreUser(user)
	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, token)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get("username")

	if username == "" {
		handleError(w, errors.New("No username provided"))
		return
	}

	user, err := mongodb.GetStoreUser(username, getAuthToken(r))
	if err != nil {
		handleError(w, err)
		return
	}

	writeJSONResponse(w, user)
}

func updateUser(w http.ResponseWriter, r *http.Request) {

}

func userHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		createUser(w, r)
	} else if r.Method == "GET" {
		getUser(w, r)
	} else if r.Method == "PATCH" {
		updateUser(w, r)
	} else {
		handleInvalidRequestMethod(w)
	}
}

func initUserRoutes() {
	http.HandleFunc("/user", userHandler)
}
