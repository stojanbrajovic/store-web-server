package mongodb

// ItemsCollection - name of the collection for store items
const ItemsCollection = "items"

// ParametersCollection - name of the collection for store item parameters
const ParametersCollection = "parameters"

// ParameterValuesCollection - contains parameter values
const ParameterValuesCollection = "parameterValues"
