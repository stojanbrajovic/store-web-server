package mongodb

import (
	"store-web-server/internal/pkg/models"

	"github.com/mongodb/mongo-go-driver/bson/objectid"
)

// CreateParameterValue creates new parameter value
func CreateParameterValue(paramValueModel models.ParameterValue, dbName string, token string) (string, error) {
	var paramValue parameterValue
	paramID, err := objectid.FromHex(paramValue.ParameterID)
	if err != nil {
		return "", err
	}
	paramValue.BsonParameterID = &paramID
	return Create(paramValue, ParameterValuesCollection, dbName, token)
}

// GetParameterValue - put parameter value in provided paramValue pointer
func GetParameterValue(id string, dbName string, token string) (*models.ParameterValue, error) {
	var paramValue parameterValue
	err := GetOne(id, ParameterValuesCollection, dbName, &paramValue, &paramValue.ParameterValue.DatabaseObject)
	if err != nil {
		return nil, err
	}

	paramValue.ParameterValue.ParameterID = getObjectIDString(paramValue.BsonParameterID)
	return &paramValue.ParameterValue, err
}

func getManyParameterValues(ids []string, storeName string) ([]*models.ParameterValue, error) {
	paramValues, err := GetMany(ids, ParameterValuesCollection, storeName, func() (interface{}, *models.DatabaseObject) {
		var paramValue parameterValue
		return &paramValue, &paramValue.DatabaseObject
	})

	if err != nil {
		return nil, err
	}

	result := make([]*models.ParameterValue, len(ids))
	for i, val := range paramValues {
		param, ok := val.(*parameterValue)
		if ok {
			param.ParameterValue.ParameterID = getObjectIDString(param.BsonParameterID)
			result[i] = &param.ParameterValue
		} else {
			return nil, errTypeConversion
		}
	}

	return result, err
}
