package mongodb

import "errors"

var errInvalidPermissions = errors.New("Invalid permission")
var errTypeConversion = errors.New("Type conversion error")
