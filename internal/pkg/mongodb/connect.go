package mongodb

import (
	"context"

	"github.com/mongodb/mongo-go-driver/mongo"
)

// ConnectToDB - connect to provided database with username and password
func connectToDB(user dbUser, dbName string) (*mongo.Database, error) {
	uri := "mongodb://" + user.username + ":" + user.password + "@localhost:27017/" + dbName
	client, err := mongo.Connect(context.TODO(), uri, nil)

	if err != nil {
		return nil, err
	}

	db := client.Database(dbName)
	return db, nil
}
