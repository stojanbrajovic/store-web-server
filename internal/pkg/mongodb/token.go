package mongodb

import (
	"context"
	"errors"
	"time"

	"github.com/mongodb/mongo-go-driver/bson"

	"github.com/dgrijalva/jwt-go"
	"github.com/mongodb/mongo-go-driver/mongo"
)

const tokensCollection = "tokens"

var tokenKey = []byte("signTokenString")

type userToken struct {
	Token string `bson:"token"`
}

func createJwtToken(username string, db *mongo.Database) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    username,
		ExpiresAt: time.Now().Add(24 * time.Hour).Unix(),
	})
	tokenString, err := token.SignedString(tokenKey)

	tokenStruct := userToken{tokenString}
	db.Collection(tokensCollection).InsertOne(context.TODO(), tokenStruct)

	return tokenString, err
}

var errInvalidToken = errors.New("Invalid token")

// TODO - return user or user id
func getUsernameFromTokenString(tokenString string) (string, error) {
	token, err := jwt.ParseWithClaims(tokenString, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return tokenKey, nil
	})

	if err != nil {
		return "", err
	}

	if token.Claims.Valid() != nil {
		return "", errInvalidToken
	}

	claims, castOk := token.Claims.(*jwt.StandardClaims)

	if !castOk {
		return "", errors.New("cast failed")
	}

	return claims.Issuer, err
}

// ValidateToken - check if token is valid
func ValidateToken(tokenString string) (string, error) {
	db, err := connectToDB(storeUsersAdmin, storeUsersDatabase)
	if err != nil {
		return "", err
	}

	collection := db.Collection(tokensCollection)
	tokenBsonFindDocument := bson.NewDocument(bson.EC.String("token", tokenString))
	err = collection.FindOne(context.TODO(), tokenBsonFindDocument).Decode(bson.NewDocument())

	if err == mongo.ErrNoDocuments {
		return "", errInvalidToken
	}

	username, err := getUsernameFromTokenString(tokenString)

	if err != nil {
		collection.DeleteOne(context.TODO(), tokenBsonFindDocument)
	}

	return username, err
}
