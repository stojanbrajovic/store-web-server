package mongodb

import (
	"context"
	"errors"
	"fmt"
	"store-web-server/internal/pkg/models"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/objectid"
	"github.com/mongodb/mongo-go-driver/mongo"
)

func canUserTokenEdit(token string, store string) bool {
	return true
}

var readUser = dbUser{"readUser", "readUser"}
var adminUser = dbUser{"admin", "readWrite"}

// Create - create new item to provided collection
func Create(item interface{}, collection string, store string, token string) (string, error) {
	if !canUserTokenEdit(token, store) {
		return "", errInvalidPermissions
	}

	return createWithoutAuth(item, collection, store)
}

func createWithoutAuth(item interface{}, collection string, storeName string) (string, error) {
	db, err := connectToDB(adminUser, storeName)
	id := ""
	if err != nil {
		return id, err
	}

	insertResult, err := db.Collection(collection).InsertOne(context.TODO(), item)

	if err == nil {
		objectID := insertResult.InsertedID.(objectid.ObjectID)
		id = getObjectIDString(&objectID)
	}

	return id, err
}

func decodeDocumentResult(docRes *mongo.DocumentResult, outs ...interface{}) error {
	itemBson := bson.NewDocument()
	err := docRes.Decode(itemBson)

	if err != nil {
		return err
	}

	data, err := itemBson.MarshalBSON()
	if err != nil {
		return err
	}

	if err != nil {
		return err
	}

	for _, out := range outs {
		err = bson.Unmarshal(data, out)
		if err != nil {
			return err
		}
	}
	return err
}

func getOneFromCollection(id string, collection *mongo.Collection, out interface{}, dbObject *models.DatabaseObject) error {
	objectID, err := objectid.FromHex(id)

	if err != nil {
		return err
	}

	docResult := collection.FindOne(context.TODO(), bson.NewDocument(bson.EC.ObjectID("_id", objectID)))
	var bsonObject Object
	err = decodeDocumentResult(docResult, out, dbObject, &bsonObject)

	if err == nil && bsonObject.BsonID != nil {
		dbObject.ID = getObjectIDString(bsonObject.BsonID)
	}

	return err
}

// GetOne - get item with provided id from collection and store it in out pointer
func GetOne(id string, collectionName string, databaseName string, out interface{}, dbObject *models.DatabaseObject) error {
	db, err := connectToDB(readUser, databaseName)

	if err != nil {
		return err
	}

	collection := db.Collection(collectionName)
	return getOneFromCollection(id, collection, out, dbObject)
}

// CreateModelFunction - function used to create models for result in GetMany function
type CreateModelFunction func() (interface{}, *models.DatabaseObject)

// GetMany - get a number of items from db with provided ids
func GetMany(ids []string, collectionName string, databaseName string, createModel CreateModelFunction) ([]interface{}, error) {
	db, err := connectToDB(readUser, databaseName)

	if err != nil {
		return nil, err
	}

	collection := db.Collection(collectionName)

	type getOneResult struct {
		model interface{}
		err   error
		index int
	}

	sem := make(chan getOneResult, len(ids))
	for i, id := range ids {
		go func(i int, id string) {
			model, dbObj := createModel()
			err := getOneFromCollection(id, collection, model, dbObj)
			sem <- getOneResult{model, err, i}
		}(i, id)
	}

	errors := make([]error, len(ids))
	models := make([]interface{}, len(ids))
	for i := 0; i < len(ids); i++ {
		result := <-sem
		if errors[result.index] != nil {
			return nil, err
		}
		models[result.index] = result.model
	}

	return models, err
}

// Delete items with ids in the ids array
func Delete(id string, collectionName string, databaseName string, token string) error {
	if !canUserTokenEdit(token, databaseName) {
		return errInvalidPermissions
	}

	db, err := connectToDB(readUser, databaseName)
	if err != nil {
		return err
	}

	collection := db.Collection(collectionName)
	objectID, err := objectid.FromHex(id)
	if err != nil {
		return err
	}

	deleteResult, err := collection.DeleteMany(context.TODO(), bson.NewDocument(bson.EC.ObjectID("_id", objectID)))
	if err != nil {
		return err
	}

	if deleteResult.DeletedCount != 1 {
		erorrText := fmt.Sprintf("Deleted %d elements", deleteResult.DeletedCount)
		err = errors.New(erorrText)
	}

	return err
}

func updateOne(id objectid.ObjectID, updateData interface{}, collectionName string, databaseName string, token string) error {
	if !canUserTokenEdit(token, databaseName) {
		return errInvalidPermissions
	}

	db, err := connectToDB(readUser, databaseName)
	if err != nil {
		return err
	}

	collection := db.Collection(collectionName)
	updateDocument, err := mongo.TransformDocument(updateData)
	if err != nil {
		return err
	}

	_, err = collection.UpdateOne(context.TODO(),
		bson.NewDocument(bson.EC.ObjectID("_id", id)),
		bson.NewDocument(bson.EC.SubDocument("$set", updateDocument)))

	return err
}
