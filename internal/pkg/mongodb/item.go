package mongodb

import (
	"context"
	"store-web-server/internal/pkg/models"

	"github.com/mongodb/mongo-go-driver/bson/objectid"
)

// CreateItem - create new item in the database
func CreateItem(itemParam models.Item, token string, storeName string) (string, error) {
	var item itemModel
	item.Item = itemParam
	item.BsonParameterValueIDs = make([]*objectid.ObjectID, len(itemParam.ParameterValueIDs))
	for i, id := range itemParam.ParameterValueIDs {
		bsonID, err := objectid.FromHex(id)
		if err != nil {
			return "", err
		}

		item.BsonParameterValueIDs[i] = &bsonID
	}
	return Create(item, ItemsCollection, storeName, token)
}

// GetItem - return item from database
func GetItem(id string, storeName string) (*models.Item, error) {
	var item itemModel
	err := GetOne(id, ItemsCollection, storeName, &item, &item.Item.DatabaseObject)
	item.Item.ParameterValueIDs = make([]string, len(item.BsonParameterValueIDs))
	for i, objID := range item.BsonParameterValueIDs {
		item.Item.ParameterValueIDs[i] = getObjectIDString(objID)
	}

	return &item.Item, err
}

// GetManyItems - get multiple items
func GetManyItems(ids []string, storeName string) ([]*models.Item, error) {
	createItem := func() (interface{}, *models.DatabaseObject) {
		var item models.Item
		return &item, &item.DatabaseObject
	}

	items, err := GetMany(ids, ItemsCollection, storeName, createItem)

	if err != nil {
		return nil, err
	}

	resultItems := make([]*models.Item, len(ids))
	for index, item := range items {
		result, ok := item.(*models.Item)
		if ok {
			resultItems[index] = result
		} else {
			return nil, errTypeConversion
		}
	}
	return resultItems, err
}

// GetStoreItems - get all items from a store
func GetStoreItems(storeName string) ([]*models.Item, error) {
	db, err := connectToDB(readUser, storeName)
	if err != nil {
		return nil, err
	}

	cursor, err := db.Collection(ItemsCollection).Find(context.TODO(), nil)
	if err != nil {
		return nil, err
	}

	var result []*models.Item
	defer cursor.Close(context.TODO())
	for cursor.Next(context.TODO()) {
		var item itemModel
		err = cursor.Decode(&item)
		if err != nil {
			return nil, err
		}

		item.ID = getObjectIDString(item.BsonID)

		item.ParameterValueIDs = make([]string, len(item.BsonParameterValueIDs))
		for i, paramID := range item.BsonParameterValueIDs {
			item.ParameterValueIDs[i] = getObjectIDString(paramID)
		}

		result = append(result, &item.Item)
	}

	return result, err
}

// FullItemData - an item with all other objects related to it
type FullItemData struct {
	Item            *models.Item             `json:"item"`
	ParameterValues []*models.ParameterValue `json:"parameterValues"`
	Parameters      []*models.Parameter      `json:"parameters"`
	ChildItems      []*models.Item           `json:"childItems"`
}

// GetFullItemData return FullItemData object for provided id
func GetFullItemData(id string, storeName string) (FullItemData, error) {
	var result FullItemData
	item, err := GetItem(id, storeName)
	if err != nil {
		return result, err
	}
	result.Item = item

	childItemsErrorChan := make(chan error)
	go func() {
		children, err := GetManyItems(item.ChildrenIds, storeName)
		result.ChildItems = children
		childItemsErrorChan <- err
	}()

	parametersErrorChan := make(chan error)
	go func() {
		var err error
		result.ParameterValues, err = getManyParameterValues(item.ParameterValueIDs, storeName)
		if err != nil {
			parametersErrorChan <- err
			return
		}

		parameterIDs := make([]string, len(result.ParameterValues))
		for i, value := range result.ParameterValues {
			parameterIDs[i] = value.ParameterID
		}
		result.Parameters, err = getManyParameters(parameterIDs, storeName)

		parametersErrorChan <- err
	}()

	err = <-childItemsErrorChan
	if err != nil {
		return result, err
	}

	err = <-parametersErrorChan
	return result, err
}

// UpdateItem updates item with data provided in item model
func UpdateItem(item models.Item, storeName string, token string) error {
	id, err := objectid.FromHex(item.ID)
	if err != nil {
		return err
	}

	return updateOne(id, &item, ItemsCollection, storeName, token)
}
