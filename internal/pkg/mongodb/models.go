package mongodb

import (
	"store-web-server/internal/pkg/models"

	"github.com/mongodb/mongo-go-driver/bson/objectid"
)

// Object is a model containting bson id object
type Object struct {
	BsonID *objectid.ObjectID `bson:"_id,omitempty"`
}

type itemModel struct {
	models.Item           `bson:",inline"`
	Object                `bson:",inline"`
	BsonParameterValueIDs []*objectid.ObjectID `bson:"parameterValueIds,omitempty"`
}

type parameterValue struct {
	models.ParameterValue `bson:",inline"`
	Object                `bson:",inline"`
	BsonParameterID       *objectid.ObjectID `bson:"parameterId"`
}
