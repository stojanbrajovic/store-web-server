package mongodb

import "store-web-server/internal/pkg/models"

func getManyParameters(ids []string, storeName string) ([]*models.Parameter, error) {
	createParameter := func() (interface{}, *models.DatabaseObject) {
		var param models.Parameter
		return &param, &param.DatabaseObject
	}

	paramsResult, err := GetMany(ids, ParametersCollection, storeName, createParameter)
	if err != nil {
		return nil, err
	}

	result := make([]*models.Parameter, len(ids))
	for i, prmInterface := range paramsResult {
		param, ok := prmInterface.(*models.Parameter)
		if ok {
			result[i] = param
		} else {
			return nil, errTypeConversion
		}
	}

	return result, err
}
