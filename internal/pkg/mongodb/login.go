package mongodb

// Login - connect to provided database with username and password
func Login(username string, password string) (string, error) {
	db, err := connectToDB(storeUsersAdmin, storeUsersDatabase)
	if err != nil {
		return "", err
	}

	user, err := getStoreUserFromDb(username, db)
	if err != nil {
		return "", err
	}

	err = checkTextWithMD5Hash(password, user.Password)
	if err != nil {
		return "", err
	}

	return createJwtToken(username, db)
}
