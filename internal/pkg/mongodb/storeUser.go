package mongodb

import (
	"context"
	"errors"
	"store-web-server/internal/pkg/models"

	"github.com/mongodb/mongo-go-driver/mongo"

	"github.com/mongodb/mongo-go-driver/bson"
)

const storeUsersDatabase = "storeUsers"
const storeUsersCollection = "storeUsers"

var storeUsersAdmin = dbUser{"usersAdmin", "usersAdmin"}
var errNoUserFound = errors.New("No users found")

func getStoreUserFromDb(username string, db *mongo.Database) (*models.User, error) {
	collection := db.Collection(storeUsersCollection)
	userBson := bson.NewDocument()
	docResult := collection.FindOne(context.TODO(), bson.NewDocument(bson.EC.String("username", username)))
	err := docResult.Decode(userBson)

	if err == mongo.ErrNoDocuments {
		err = errNoUserFound
		return nil, err
	}

	var user models.User
	userData, err := userBson.MarshalBSON()
	if err != nil {
		return &user, err
	}

	err = bson.Unmarshal(userData, &user)
	return &user, err
}

// CreateStoreUser - create new user for the store
func CreateStoreUser(userArg models.User) (string, error) {
	db, err := connectToDB(storeUsersAdmin, storeUsersDatabase)
	if err != nil {
		return "", err
	}

	existingUser, err := getStoreUserFromDb(userArg.Username, db)
	if err == errNoUserFound {
		user := userArg
		user.Password = getMD5Hash(userArg.Password)
		_, err = db.Collection(storeUsersCollection).InsertOne(context.TODO(), user)
	} else if existingUser != nil {
		err = errors.New("User with that username already exists")
	}

	if err != nil {
		return "", err
	}

	return createJwtToken(userArg.Username, db)
}

// GetStoreUser - return a user from the store
func GetStoreUser(username string, token string) (*models.User, error) {
	tokenUsername, err := ValidateToken(token)
	if err != nil {
		return nil, err
	}

	if tokenUsername != username {
		return nil, errors.New("Invalid access rights")
	}

	db, err := connectToDB(storeUsersAdmin, storeUsersDatabase)
	if err != nil {
		return nil, err
	}

	return getStoreUserFromDb(username, db)
}
