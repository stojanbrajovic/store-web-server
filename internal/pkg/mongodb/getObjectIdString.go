package mongodb

import (
	"github.com/mongodb/mongo-go-driver/bson/objectid"
)

func getObjectIDString(id *objectid.ObjectID) string {
	const start = `ObjectId("`
	const end = `")`

	fullIDString := id.String()

	return fullIDString[len(start):(len(fullIDString) - len(end))]
}
