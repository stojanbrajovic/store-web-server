package mongodb

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
)

func getMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func checkTextWithMD5Hash(text string, hash string) error {
	if getMD5Hash(text) != hash {
		return errors.New("Hash doesn't match")
	}

	return nil
}
