package models

// DatabaseObject - base struct for any object in the database
type DatabaseObject struct {
	ID string `json:"id,omitempty" bson:"-"`
}
