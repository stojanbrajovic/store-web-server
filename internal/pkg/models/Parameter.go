package models

// Parameter - parameter for item in the store
type Parameter struct {
	StoreObject `json:",inline" bson:",inline"`
	IsNumeric   bool    `json:"isNumeric,omitempty" bson:"isNumeric,omitempty"`
	Maximum     float64 `json:"maximum,omitempty" bson:"maximum,omitempty"`
	Minumum     float64 `json:"minimum,omitempty" bson:"minimum,omitempty"`
	Step        float64 `json:"step,omitempty" bson:"step,omitempty"`
}
