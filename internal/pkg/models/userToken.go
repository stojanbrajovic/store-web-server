package models

// UserToken - login token saved to db
type UserToken struct {
	Token string `json:"token" bson:"token"`
}
