package models

// User - store user data
type User struct {
	ID       string `json:"id"`
	Username string `json:"username" bson:"username"`
	Password string `json:"password,omitempty" bson:"password,omitempty"`
	Email    string `json:"email,omitempty" bson:"email,omitempty"`
}
