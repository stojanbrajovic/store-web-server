package models

// StoreObject - basic struct for all objects in the store
type StoreObject struct {
	DatabaseObject `json:",inline" bson:",inline"`
	Name           string   `json:"name,omitempty" bson:"name,omitempty"`
	Description    string   `json:"description,omitempty" bson:"description,omitempty"`
	ChildrenIds    []string `json:"childrenIds,omitempty" bson:"childrenIds,omitempty"`
}
