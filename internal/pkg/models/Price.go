package models

// Price of an item
type Price struct {
	StoreObject `json:",inline" bson:",inline"`
	Amount      float64 `json:"amount" bson:"amount"`
	Currency    string  `json:"currency" bson:"currency"`
}
