package models

// ParameterValue - saved parameter value
type ParameterValue struct {
	StoreObject `json:",inline" bson:",inline"`
	Value       float64 `json:"value,omitempty" bson:"value,omitempty"`
	ParameterID string  `json:"parameterId" bson:"-"`
}
