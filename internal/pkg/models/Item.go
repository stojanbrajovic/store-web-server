package models

// Item - single store item
type Item struct {
	StoreObject       `json:",inline" bson:",inline"`
	IsBaseItem        bool     `json:"isBaseItem,omitempty" bson:"isBaseItem,omitempty"`
	ParameterValueIDs []string `json:"parameterValueIds,omitempty" bson:"-"`
	PriceIDs          []string `json:"priceIds,omitempty" bson:"-"`
}
